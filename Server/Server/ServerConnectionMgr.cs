﻿using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Collections.Generic;
using Shared;
using Server.Network;
using Server.Handlers;

namespace Server
{
    public class ServerConnectionMgr : ConnectionMgr
    {
        public static ServerConnectionMgr sConnection { get; private set; }
        private ManualResetEvent allDone = new ManualResetEvent(false);

        public Socket ServerSocket { get; private set; }

        public bool Running { get; private set; }

        public static void Start()
        {
            sConnection = new ServerConnectionMgr();

            sConnection.SetUp(new ServerHandler());

            sConnection.ServerSocket = new Socket(AddressFamily.InterNetwork,
                                SocketType.Stream,
                                ProtocolType.Tcp);

            sConnection.ServerSocket.NoDelay = true;
            try
            {
                sConnection.ServerSocket.Bind(sConnection.connection.CreateEndPoint());
                sConnection.ServerSocket.Listen(100);
                sConnection.Running = true;

                while (sConnection.Running)
                {
                    sConnection.allDone.Reset();
                    Console.WriteLine("Waiting for a new connections...");

                    sConnection.ServerSocket.BeginAccept(new AsyncCallback(sConnection.AcceptCallback), sConnection.ServerSocket);
                    sConnection.allDone.WaitOne();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public void Stop()
        {
            Running = false;
            allDone.Set();
            ClientMgr.RemoveAll();
            Connector.Disconnect(ServerSocket);
        }

        private void AcceptCallback(IAsyncResult ar)
        {
            if (Running == false)
            {
                return;
            }

            allDone.Set();

            Socket listener = (Socket)ar.AsyncState;
            Socket handler = listener.EndAccept(ar);
            handler.NoDelay = true;

            Client client = ClientMgr.AddClient(handler);
            if (client != null)
            {
                Receive(client);
                client.Sending.Set();
            }
        }
    }
}
