﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;
using Shared.Converting;
using Shared;

namespace Server.MySQL.Managers
{
    public class ClientDbMgr
    {
        private static object maxIDLocker = new object();
        private static UInt32 maxID { get; set; }

        private static UInt32 CreateMaxID()
        {
            return maxID++;
        }

        public static bool ExistsUsername(string username)
        {
            string command = @"SELECT COUNT(`id`) as `exists` FROM `users` WHERE `username` = @username";
            using (MySqlDataReader reader = Database.Query(command, new MySqlParameter("@username", username)))
            {
                if (reader.Read() && reader.GetInt32(0) != 0)
                {
                    return true;
                }
            }

            return false;
        }

        public static bool ExistsAccount(string username, string password)
        {
            return (GetAccount(username, password) != null);
        }

        public static MyAccount GetAccount(string username, string password)
        {
            string command = @"SELECT `id`, `password`, `name`, `mood` FROM `users` WHERE `username` = @username";
            using (MySqlDataReader reader = Database.Query(command, new MySqlParameter("@username", username)))
            {
                DataTable table = new DataTable();
                table.Load(reader);

                foreach(DataRow row in table.Rows)
                {
                    string _password = row.Field<string>("password");
                    if(ConvertHash.ValidatePassword(password, _password)) 
                    {
                        UInt32 id = row.Field<UInt32>("id");
                        string name = row.Field<string>("name");
                        string mood = row.Field<string>("mood");

                        UInt32 lastMessageID = ClientDbMgr.GetLastMessageID(id);
                        MyAccount account = new MyAccount(id, username, name, mood, lastMessageID);
                        return account;
                    }
                }
            }

            return null;
        }

        public static Account GetAccount(UInt32 id)
        {
            string command = @"SELECT `username`, `name`, `mood` FROM `users` WHERE `id` = @id";
            using (MySqlDataReader reader = Database.Query(command, new MySqlParameter("@id", id)))
            {
                if (reader.Read() && reader.HasRows)
                {
                    string username = reader.GetString(0);
                    string name = reader.GetString(1);
                    string mood = reader.GetString(2);

                    Account account = new Account(id, username, name, mood);
                    return account;
                }
            }

            return null;
        }

        public static void CreateAccount(string username, string password, string name, string email)
        {
            lock (maxIDLocker)
            {
                string command = @"INSERT INTO `users` (`id`, `username`, `password`, `name`, `email`) VALUES (@maxID, @username, @password, @name, @email);";
                UInt32 myID = CreateMaxID();
                Database.Execute(command,
                                 new MySqlParameter("@maxID", myID),
                                 new MySqlParameter("@username", username),
                                 new MySqlParameter("@password", password),
                                 new MySqlParameter("@name", name),
                                 new MySqlParameter("@email", email)
                                 );
            }
        }

        public static UInt32 GetLastMessageID(UInt32 userId)
        {
            string command = "SELECT MAX(`id`) as `max` FROM `messages` WHERE `id_sender` = @userId";
            using (MySqlDataReader reader = Database.Query(command, new MySqlParameter("@userId", userId)))
            { 
                if (reader.Read() && !reader.IsDBNull(0))
                {
                    return reader.GetUInt32(0);
                }
            }

            return 0;
        }

        public static void SetMaxID()
        {
            using (MySqlDataReader reader = Database.Query("SELECT MAX(`id`) as `max` FROM `users`"))
            {
                if (reader.Read() && !reader.IsDBNull(0))
                {
                    maxID = reader.GetUInt32(0) + 1;
                }
                else
                {
                    maxID = 0;
                }
            }
        }
    }
}
