﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;

namespace Server.MySQL
{
    public class Database : DatabaseData
    {
        private static MySqlConnection connection = null;

        public static bool Opened 
        { 
            get 
            { 
                return connection != null; 
            }
        }

        public static void Open()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat
                (
                    "SERVER={0}; DATABASE={1}; UID={2}; PASSWORD={3};", Address, Database, Username, Password
                );

            try
            {
                connection = new MySqlConnection(sb.ToString());
                connection.Open();
            }
            catch
            {
                connection = null;
            }
        }

        public static void Close()
        {
            if (connection != null)
            {
                if (connection.State != ConnectionState.Closed)
                {
                    try
                    {
                        connection.Close();
                    }
                    catch
                    {
                        connection = null;
                    }
                }
            }
        }

        public static void Execute(string execute, params MySqlParameter[] values)
        {
            if (connection != null && connection.State == ConnectionState.Open)
            {
                try
                {
                    MySqlCommand mySqlCommand = new MySqlCommand(execute, connection);
                    foreach (MySqlParameter value in values)
                    {
                        mySqlCommand.Parameters.Add(value);
                    }

                    mySqlCommand.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        public static MySqlDataReader Query(string query, params MySqlParameter[] values)
        {
            if (connection != null && connection.State == ConnectionState.Open)
            {
                try
                {
                    MySqlCommand mySqlCommand = new MySqlCommand(query, connection);
                    foreach (MySqlParameter value in values)
                    {
                        mySqlCommand.Parameters.Add(value);
                    }

                    return mySqlCommand.ExecuteReader();
                }
                catch(Exception e)
                {
                    throw e;
                }
            }

            return null;
        }
    }
}
