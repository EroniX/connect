﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Shared.Tasks;
using Server.Network;
using Server;

namespace Server.Tasks
{
    class ServerTaskMgr
    {
        private static TaskMgr taskMgr;

        public static void Start()
        {
            ServerTaskMgr.taskMgr = new TaskMgr();
            ServerTaskMgr.taskMgr.Start(ServerConnectionMgr.Start);
        }

        public static void Stop()
        {
            ServerTaskMgr.taskMgr.Stop();
            CommandTaskMgr.Stop();
        }
    }
}
