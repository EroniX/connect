﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared.Tasks;

namespace Server.Tasks
{
    class CommandTaskMgr
    {
        private static TaskMgr taskMgr;

        public static void Start()
        {
            CommandTaskMgr.taskMgr = new TaskMgr();
            CommandTaskMgr.taskMgr.Start(CommandMgr.DoCommand);
        }

        public static void Stop()
        {
            CommandTaskMgr.taskMgr.Stop();
        }
    }
}
