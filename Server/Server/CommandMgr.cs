﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared;
using Server.Tasks;
using Server.Network;

namespace Server
{
    class CommandMgr
    {
        public static void DoCommand()
        {
            while(true)
            {
                string s = Console.ReadLine();
                switch(s)
                {
                    case "SEND":
                        string message = "Server message!";
                        /*PacketBuilder response = new PacketBuilder(PacketType.S_NEW_MESSAGE);
                        response.Set(0, (UInt16)0);
                        response.Set(1, (UInt16)message.Length);
                        response.Set(2, message);
                        ClientMgr.SendToAll(response);*/
                        break;
                    case "DISCONNECT":
                        ConnectionMgr.RemoveAll();
                        break;
                    case "CLOSE":
                        ServerTaskMgr.Stop();
                        CommandTaskMgr.Stop();
                        return;
                }
            }
        }
    }
}
