﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared;
using Server.MySQL.Managers;

namespace Server.Network
{
    public class AccountMgr
    {
        public bool LoggedIn { get; private set; }
        public MyAccount Datas { get; private set; }

        public AccountMgr()
        {
            this.LoggedIn = false;
        }

        public AccountMgr(MyAccount account)
        {
            this.Datas = account;
            this.LoggedIn = true;
        }

        public bool LogIn(string username, string password)
        {
            if(LoggedIn == false)
            {
                MyAccount account = ClientDbMgr.GetAccount(username, password);
                if(account != null)
                {
                    this.LoggedIn = true;
                    this.Datas = account;
                    return true;
                }
            }

            return false;
        }

        public void LogOut()
        {
            this.Datas = null;
        }
    }
}
