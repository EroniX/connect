﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using Shared;

namespace Server.Network
{
    class ClientMgr
    {
        private static readonly object maxIDLocker = new object();
        private static int maxID = 0;

        public static readonly int MAX_CONNECTED_ON_SAME_COMPUTER = 2;
        public static List<Client> clients = new List<Client>();

        public static Client AddClient(Socket socket)
        {
            IPEndPoint endPoint = socket.RemoteEndPoint as IPEndPoint;
            int counter = 0;
            foreach(Client client in clients) 
            {
                IPEndPoint clientEndPoint = client.Socket.RemoteEndPoint as IPEndPoint;
                if (endPoint.Address.Equals(clientEndPoint.Address)) {
                    counter++;
                }
            }

            if (counter == MAX_CONNECTED_ON_SAME_COMPUTER) {
                Connector.Disconnect(socket);
                return null;
            }

            lock (maxIDLocker)
            {
                Client client = new Client(ClientMgr.maxID, socket);
                ClientMgr.IncreaseID();
                clients.Add(client);
                return client;
            }
        }

        public static void RemoveClient(Client client)
        {
            Connector.Disconnect(client.Socket);
            clients.Remove(client);
        }

        public static void RemoveAll()
        {
            foreach (Client client in clients) {
                Connector.Disconnect(client.Socket);
            }

            clients = new List<Client>();
        }

        public static void SendToAll(PacketBuilder packet)
        {
            foreach (Client client in clients) {
                ServerConnectionMgr.Send(client, packet);
            }
        }

        public static void SendToAllExcept(Client except, PacketBuilder packet)
        {
            foreach (Client client in clients) {
                if (except != client) {
                    ServerConnectionMgr.sConnection.Send(client, packet);
                }
            }
        }

        public static int MaxID
        {
            get
            {
                return ClientMgr.maxID;
            }
        }

        public static void IncreaseID()
        {
            ClientMgr.maxID++;
        }
    }
}
