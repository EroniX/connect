﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared;

namespace Server.Network
{
    public class ServerPacket : Packet
    {
        public Client Client { get; private set; }

        public ServerPacket(Client client, byte[] content, PacketType packetType)
            : base(content, packetType)
        {
            this.Client = client;
        }
    }
}
