﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared;

namespace Server.Network
{
    public class ServerData : Data
    {
        public Client Client;

        public ServerData(Client client, List<byte> packet) 
            : base()
        {
            this.Client = client;
        }

        public ServerData(Client client)
            : this(client, new List<byte>())
        { }
    }
}
