﻿using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Collections.Generic;
using Server.Tasks;
using Shared;
using Server.MySQL;
using Server.MySQL.Managers;

class Program
{
    public static void Main(String[] args)
    {
        Database.Open();
        if (Database.Opened)
        {
            DbMgr.SetUp();
            CommandTaskMgr.Start();
            ServerTaskMgr.Start();
        }
    }
}