﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared;
using Server.Network;

namespace Server.Handlers
{
    public class ServerHandler : IHandler
    {
        private readonly List<HandlerObject> HandlerList = new List<HandlerObject>
        {
            new HandlerObject(PacketType.C_LOGIN,        UserHandler.HandleLogin),
            new HandlerObject(PacketType.C_REGISTER,     UserHandler.HandleRegister)
        };

        public void HandleData(Packet packet)
        {
            Action<Packet> action = Handler.GetHandler(HandlerList, packet.PacketObject.PacketType);
            if (action != null)
            {
                ServerPacket sPacket = packet as ServerPacket;
                if (sPacket != null)
                {
                    action.Invoke(packet);
                }
                else
                {
                    // Lost packet!
                }
            }
            else
            {
                action = BasicHandler.HandleUnknown;
                action.Invoke(null);
            }
        }
    }
}
