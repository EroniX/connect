﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared;
using Server.Network;
using Shared.Converting;
using Server.MySQL.Managers;

namespace Server.Handlers
{
    public class UserHandler
    {
        public static void HandleLogin(Packet packet)
        {
            ServerPacket sPacket = packet as ServerPacket;
            if(sPacket == null)
            {
                // @TODO: Error message
                return;
            }

            string username = sPacket.Get<string>(1);
            string password = sPacket.Get<string>(3);

            if (Validate.ValidateUsername(username) && sPacket.Client.Account.LogIn(username, password))
            {
                string name = sPacket.Client.Account.Datas.Name;
                string mood = sPacket.Client.Account.Datas.Mood;

                PacketBuilder response = new PacketBuilder(PacketType.S_LOGIN_SUCCESS);
                response.Set<UInt32>(0, sPacket.Client.Account.Datas.Id);
                response.Set<byte>(1, ConvertValue.GetShortStringLength(username));
                response.Set<string>(2, username);
                response.Set<byte>(3, ConvertValue.GetShortStringLength(name));
                response.Set<string>(4, name);
                response.Set<byte>(5, ConvertValue.GetShortStringLength(mood));
                response.Set<string>(6, mood);
                response.Set<UInt32>(7, sPacket.Client.Account.Datas.LastMessageID);
                sPacket.Client.Send(response);
            }
            else
            {
                PacketBuilder response = new PacketBuilder(PacketType.S_LOGIN_FAILED);
                response.Set<byte>(0, 0);
                sPacket.Client.Send(response);
            }
        }

        public static void HandleRegister(Packet packet)
        {
            ServerPacket sPacket = packet as ServerPacket;
            if (sPacket == null)
            {
                // @TODO: Error message
                return;
            }

            string username = sPacket.Get<string>(1);
            string password = sPacket.Get<string>(3);
            string name = sPacket.Get<string>(5);
            string email = sPacket.Get<string>(7);

            bool validUser = Validate.ValidateUsername(username);
            bool validPass = Validate.ValidatePassword(password);
            bool freeUser = !ClientDbMgr.ExistsUsername(username);

            if (validUser && validPass && freeUser)
            {
                string hashedPassword = ConvertHash.CreateHash(password);
                MySQL.Managers.ClientDbMgr.CreateAccount(username, hashedPassword, name, email);

                if (sPacket.Client.Account.LogIn(username, password))
                {
                    PacketBuilder response = new PacketBuilder(PacketType.S_LOGIN_SUCCESS);
                    response.Set<UInt32>(0, sPacket.Client.Account.Datas.Id);
                    response.Set<byte>(1, ConvertValue.GetShortStringLength(username));
                    response.Set<string>(2, username);
                    response.Set<byte>(3, ConvertValue.GetShortStringLength(name));
                    response.Set<string>(4, name);
                    response.Set<byte>(5, ConvertValue.GetShortStringLength(""));
                    response.Set<string>(6, "");
                    response.Set<UInt32>(7, sPacket.Client.Account.Datas.LastMessageID);
                    sPacket.Client.Send(response);
                }
                else
                {
                    // @TODO: Something went badly wrong
                }
            }
            else
            {
                PacketBuilder response = new PacketBuilder(PacketType.S_REGISTER_FAILED);
                response.Set<byte>(0, (byte)(freeUser == false ? 1 : 0));
                sPacket.Client.Send(response);
            }
        }
    }
}
