﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared;
using Server.Network;

namespace Server.Handlers
{
    public class BasicHandler
    {
        public static void HandleUnknown(Packet packet)
        {
            Console.WriteLine("HandleUnknown");
        }

        public static void HandleJoin(Packet packet)
        {
            Console.WriteLine("HandleJoin");
        }

        public static void HandleLeave(Packet packet)
        {
            Console.WriteLine("HandleLeave");
        }
    }
}
