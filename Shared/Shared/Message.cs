﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared
{
    public class Message
    {
        public int Id { get; private set; }
        public int IdSender { get; private set; }
        public int IdReceiver { get; private set; }
        public string Content { get; private set; }
        public DateTime Date { get; private set; }
        public bool Confirmed { get; private set; }
    }
}
