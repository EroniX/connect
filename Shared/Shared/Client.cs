﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;

namespace Shared
{
    public class Client : Connector
    {
        //public AccountMgr Account { get; private set; }
        private bool connected = false;
        private int id;
        public Account Account { get; private set; }

        public Client(int id, Socket socket)
            : base(socket)
        { }
    }
}
