﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared
{
    public class Account
    {
        public UInt32 Id { get; private set; }
        public string Username { get; private set; }
        public string Name { get; private set; }
        public string Mood { get; private set; }

        public Account(UInt32 id, string username, string name, string mood)
        {
            this.Id = id;
            this.Username = username;
            this.Name = name;
            this.Mood = mood;
        }

        public void LoadMessages()
        {

        }
    }
}
