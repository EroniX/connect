﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;

namespace Shared
{
    public class Connection
    {
        protected string host;
        protected int port;

        public int Port
        {
            get
            {
                return this.port;
            }
        }
        public string Host
        {
            get
            {
                return this.host;
            }
        }

        public void Initialize()
        {
            using (StreamReader sr = new StreamReader("connect.txt"))
            {
                this.host = sr.ReadLine();
                this.port = int.Parse(sr.ReadLine());
            }
        }
        public IPEndPoint CreateEndPoint()
        {
            IPHostEntry ipHostInfo = Dns.Resolve(host);
            IPAddress ipAddress = ipHostInfo.AddressList[0];
            return (new IPEndPoint(ipAddress, port));
        }
    }
}
