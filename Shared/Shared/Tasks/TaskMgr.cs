﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Shared.Tasks
{
    public class TaskMgr
    {
        public bool Started = false;
        public Thread Thread;

        public void Start(ThreadStart threadStart)
        {
            if (this.Started == false)
            {
                this.Thread = new Thread(threadStart);
                this.Thread.Start();
                this.Started = true;
            }
        }

        public void Stop()
        {
            if (this.Started == true)
            {
                this.Thread.Abort();
                this.Thread.Join();
                this.Started = false;
            }
        }
    }
}
