﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared
{
    public class Validate
    {
        public static bool ValidateUsername(string username)
        {
            return !Array.Exists(username.ToCharArray(), n => 
                        {
                            return !char.IsLetter(n) && !char.IsDigit(n);
                        });
        }

        public static bool ValidatePassword(string password)
        {
            return !Array.Exists(password.ToCharArray(), n =>
                        {
                            return !char.IsLetter(n) && !char.IsDigit(n);
                        });
        }

        public static bool ValidateEmail(string email)
        {
            try
            {
                new System.Net.Mail.MailAddress(email);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
