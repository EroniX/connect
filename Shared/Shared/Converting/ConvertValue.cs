﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared
{
    public class ConvertValue
    {
        public static byte[] Get(object content)
        {
            if (content.GetType() == typeof(byte))
                return ConvertValue.GetUInt8((byte)content);
            else if (content.GetType() == typeof(UInt16))
                return GetUInt16((UInt16)content);
            else if (content.GetType() == typeof(UInt32))
                return GetUInt32((UInt32)content);
            else if (content.GetType() == typeof(UInt64))
                return GetUInt64((UInt64)content);
            else if (content.GetType() == typeof(sbyte))
                return GetInt8((sbyte)content);
            else if (content.GetType() == typeof(Int16))
                return GetInt16((Int16)content);
            else if (content.GetType() == typeof(Int32))
                return GetInt32((Int32)content);
            else if (content.GetType() == typeof(Int64))
                return GetInt64((Int64)content);
            else if (content.GetType() == typeof(float))
                return GetFloat((float)content);
            else if (content.GetType() == typeof(double))
                return GetDouble((double)content);
            else if (content.GetType() == typeof(char))
                return GetChar((char)content);
            else
                return GetString((string)content);
        }

        public static byte[] GetUInt8(byte value)
        {
            return new byte[] { value };
        }

        public static byte[] GetUInt16(UInt16 value)
        {
            return BitConverter.GetBytes(value);
        }

        public static byte[] GetUInt32(UInt32 value)
        {
            return BitConverter.GetBytes(value);
        }

        public static byte[] GetUInt64(UInt64 value)
        {
            return BitConverter.GetBytes(value);
        }

        public static byte[] GetInt8(sbyte value)
        {
            return BitConverter.GetBytes(value);
        }

        public static byte[] GetInt16(Int16 value)
        {
            return BitConverter.GetBytes(value);
        }

        public static byte[] GetInt32(Int32 value)
        {
            return BitConverter.GetBytes(value);
        }

        public static byte[] GetInt64(Int64 value)
        {
            return BitConverter.GetBytes(value);
        }

        public static byte[] GetFloat(float value)
        {
            return BitConverter.GetBytes(value);
        }

        public static byte[] GetDouble(double value)
        {
            return BitConverter.GetBytes(value);
        }

        public static byte[] GetChar(char value)
        {
            return BitConverter.GetBytes(value);
        }

        public static byte[] GetString(string value)
        {
            return UTF8Encoding.UTF8.GetBytes(value);
        }

        public static UInt16 GetLongStringLength(string value)
        {
            return Convert.ToUInt16(Encoding.UTF8.GetByteCount(value));
        }

        public static byte GetShortStringLength(string value)
        {
            return Convert.ToByte(Encoding.UTF8.GetByteCount(value));
        }
    }
}
