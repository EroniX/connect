﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared
{
    public class ConvertByte
    {
        public static object Get(byte[] content, Type type)
        {
            if (type == typeof(byte))
                return GetUInt8(content);
            else if (type == typeof(UInt16))
                return GetUInt16(content);
            else if (type == typeof(UInt32))
                return GetUInt32(content);
            else if (type == typeof(UInt64))
                return GetUInt64(content);
            else if (type == typeof(sbyte))
                return GetInt8(content);
            else if (type == typeof(Int16))
                return GetInt16(content);
            else if (type == typeof(Int32))
                return GetInt32(content);
            else if (type == typeof(Int64))
                return GetInt64(content);
            else if (type == typeof(float))
                return GetFloat(content);
            else if (type == typeof(double))
                return GetDouble(content);
            else if (type == typeof(char))
                return GetChar(content);
            else
                return GetString(content);
        }

        public static byte GetUInt8(byte[] content)
        {
            return content[0];
        }

        public static UInt16 GetUInt16(byte[] content)
        {
            return BitConverter.ToUInt16(content, 0);
        }

        public static UInt32 GetUInt32(byte[] content)
        {
            return BitConverter.ToUInt32(content, 0);
        }

        public static UInt64 GetUInt64(byte[] content)
        {
            return BitConverter.ToUInt64(content, 0);
        }

        public static sbyte GetInt8(byte[] content)
        {
            return Convert.ToSByte(content[0]);
        }

        public static Int16 GetInt16(byte[] content)
        {
            return BitConverter.ToInt16(content, 0);
        }

        public static Int32 GetInt32(byte[] content)
        {
            return BitConverter.ToInt32(content, 0);
        }

        public static Int64 GetInt64(byte[] content)
        {
            return BitConverter.ToInt64(content, 0);
        }

        public static float GetFloat(byte[] content)
        {
            return BitConverter.ToSingle(content, 0);
        }

        public static double GetDouble(byte[] content)
        {
            return BitConverter.ToDouble(content, 0);
        }

        public static string GetString(byte[] content)
        {
            return UTF8Encoding.UTF8.GetString(content, 0, content.Length);
        }

        public static char GetChar(byte[] content)
        {
            return BitConverter.ToChar(content, 0);
        }
    }
}
