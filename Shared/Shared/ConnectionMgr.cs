﻿using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Collections.Generic;
using Shared;

namespace Shared
{
    public class ConnectionMgr
    {
        protected Connection connection { get; private set; }
        private IHandler handler { get; set; }

        protected void SetUp(IHandler handler)
        {
            connection = new Connection();
            connection.Initialize();
            this.handler = handler;
        }

        protected void Receive(Client client)
        {
            try
            {
                Data data = new Data(client);
                client.Socket.BeginReceive(data.Buffer, 0, Data.BufferSize, 0,
                    new AsyncCallback(ReceiveCallback), data);
            }
            catch (Exception e)
            {
                //Console.WriteLine(e.ToString());
            }
        }

        private void ReceiveCallback(IAsyncResult ar)
        {
            Data data = ar.AsyncState as Data;
            Client client = data.Client;

            int bytesRead = 0;
            try
            {
                bytesRead = client.Socket.EndReceive(ar);
            }
            catch
            {
               /* if (Connector.IsConnected(client.Socket) == false)
                {
                    Console.WriteLine("Removed client!");
                    //ClientMgr.RemoveClient(client);
                    return;
                }*/
            }

            if (bytesRead > 0)
            {
                data.Packet.AddRange(data.Buffer
                    .Take(bytesRead));

                if (data.GetSize() == 0 && data.Packet.Count() >= 2) {
                    data.SetSize();
                }

                if (data.GetSize() == data.Packet.Count())
                {
                    Receive(client);
                    data.SetPacketType();
                    Packet packet = new Packet(client, data.GetContent(), data.GetPacketType());
                    //ServerHandler.HandlePacket(packet);
                }
                else if (data.GetSize() < data.Packet.Count())
                {
                    do
                    {
                        List<byte> myPacket = data.Packet.Take(data.GetSize()).ToList();
                        Data myData = new Data(client, myPacket);

                        myData.SetSize(data.GetSize());
                        myData.SetPacketType();

                        Packet packet = new Packet(client, data.GetContent(), myData.GetPacketType());
                        //ServerHandler.HandlePacket(packet);

                        data.Packet = data.Packet.Skip(data.GetSize()).ToList();
                        if (data.Packet.Count() >= 2)
                        {
                            data.SetSize();
                        }
                    }
                    while (data.GetSize() >= data.Packet.Count() && data.Packet.Count() != 0);

                    if (data.GetSize() == 0) {
                        Receive(client);
                    }
                    else {
                        client.Socket.BeginReceive(data.Buffer, 0, Data.BufferSize, 0,
                            new AsyncCallback(ReceiveCallback), data);
                    }
                }
                else
                {
                    client.Socket.BeginReceive(data.Buffer, 0, Data.BufferSize, 0,
                            new AsyncCallback(ReceiveCallback), data);
                }
            }
        }

        public void Send(Client client, PacketBuilder packet)
        {
            try
            {
                client.Sending.WaitOne(200);
                client.Sending.Reset();

                List<byte> finish = packet.Finish();
                client.Socket.BeginSend(finish.ToArray(), 0, finish.Count(), 0,
                    new AsyncCallback(SendCallback), client);
            }
            catch
            {
                //ClientMgr.RemoveClient(this);
            }
        }

        private void SendCallback(IAsyncResult ar)
        {
            try
            {
                Client client = (Client)ar.AsyncState;
                int bytesSent = client.Socket.EndSend(ar);
                client.Sending.Set();
            }
            catch
            {
                //ClientMgr.RemoveClient(this);
            }
        }
    }
}
