﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared
{
    public class Friend : Account
    {
        private List<Message> messages = new List<Message>();

        public Friend(Account account)
            : base(account.Id, account.Username, account.Name, account.Mood)
        { }

        public Friend(UInt32 id, string username, string name, string mood)
            : base(id, username, name, mood)
        { }
    }
}
