﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared;

namespace Shared
{
    public class Data
    {
        public const int BufferSize = 8192;

        private PacketType packetType { get; set; }
        private UInt16 size { get; set; }
        public byte[] Buffer { get; set; }
        public List<byte> Packet { get; set; }
        public Client Client { get; private set; }

        public Data(Client client, List<byte> packet)
        {
            this.Client = client;
            this.Packet = packet;
            this.Buffer = new byte[BufferSize];
            this.packetType = PacketType.UNKNOWN;
            this.size = 0;
        }

        public Data(Client client)
            : this(client, new List<byte>())
        { }

        public void SetSize()
        {
           this.SetSize(BitConverter.ToUInt16(this.Packet.Take(2).ToArray(), 0));
        }

        public void SetSize(UInt16 size)
        {
            this.size = size;
        }

        public UInt16 GetSize()
        {
            return this.size;
        }

        public void SetPacketType()
        {
            UInt16 value = BitConverter.ToUInt16(this.Packet.Skip(2).Take(2).ToArray(), 0);
            this.packetType = (value >= (UInt16)PacketType.MAX_PACKET) ? PacketType.UNKNOWN : (PacketType)value;
        }

        public PacketType GetPacketType()
        {
            return this.packetType;
        }

        public byte[] GetContent()
        {
            return this.Packet.Skip(4).ToArray();
        }
    }
}
