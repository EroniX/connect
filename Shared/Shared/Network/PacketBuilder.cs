﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace Shared
{
    public class PacketBuilder
    {
        //public Client Client { get; private set; }
        public PacketType PacketType { get; private set; }
        private object[] packetData { get; set; }

        public PacketBuilder(/*Client client, */PacketType packetType)
        {
            //this.Client = client;
            this.PacketType = packetType;
            this.packetData = Enumerable.Repeat<object>(null, PacketTypeContainer.CountElements(packetType)).ToArray();
        }

        public void Set<T>(int index, T value)
        {
            this.packetData[index] = value;
        }

        public List<byte> Finish()
        {
            List<byte> content = new List<byte>();
            PacketObject packetObject = PacketTypeContainer.GetElement(this.PacketType);

            for (int i = 0; i < packetData.Length; i++)
            {
                if(packetData[i] == null)
                {
                    return content;
                }

                int expectedBytes;
                if (packetObject.DataList[i] == typeof(string))
                {
                    expectedBytes = Convert.ToInt32(packetData[i - 1]);
                }
                else
                {
                    expectedBytes = Marshal.SizeOf(packetObject.DataList[i]);
                }
                byte[] convertedBytes = ConvertValue.Get(packetData[i]);

                if(expectedBytes != 0 && convertedBytes.Length != expectedBytes)
                {
                    return new List<byte>();
                }
                else
                {
                    content.AddRange(convertedBytes);
                }
            }

            UInt16 size = Convert.ToUInt16(content.Count() + 4);
            content.InsertRange(0, BitConverter.GetBytes((UInt16)this.PacketType).ToList());
            content.InsertRange(0, BitConverter.GetBytes(size).ToList());
            
            return content;
        }
    }
}
