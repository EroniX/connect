﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared
{
    public enum PacketType : ushort
    {
        UNKNOWN                  = 0,  
        //
        C_LOGIN                  = 1,  
        // UInt8(Length of Username) - String(Username) - UInt8(Length of Password) - String(Password)
        S_LOGIN_SUCCESS          = 2,  
        // UInt32(ID) - UInt8(Length of Username) - String(Username) - UInt8(Length of Name) - String(Name) - UInt8(Length of Mood) - String(Mood) - UInt32(Last Message ID)
        S_LOGIN_FAILED           = 3,  
        // UInt8(Error code)
        C_REGISTER               = 4,  
        // UInt8(Length of Username) - String(Username) - UInt8(Length of Password) - String(Password) - UInt8(Length of Name) - String(Name) - UInt8(Length of Email) - String(Email)
        S_REGISTER_FAILED        = 5,
        // UInt8(Error code)
        C_SEND_MESSAGE           = 6,  
        // UInt32(ID of Receiver) - UInt16(Length of Message) - String(Message)
        S_SEND_MESSAGE           = 7,
        // UInt32(ID of Sender) - UInt16(Length of Message) - String(Message)
        S_MESSAGE_FAILED         = 8,
        // UInt32(Message ID) - UInt8(Error code)
        C_FRIEND_REQUEST         = 9, 
        // UInt32(ID of Friend)
        C_FRIEND_REMOVE          = 10,
        // UInt32(ID of Friend)
        C_FRIEND_IGNORE          = 11,
        // UInt32(ID of Friend)
        S_FRIEND                 = 12,
        // UInt32(Id of Friend) - UInt8(Length of Username) - String(Username) - UInt8(Length of Name) - String(Name) - UInt8(Length of Mood) - String(Mood)
        MAX_PACKET               = 13 
        //
    }

    public class PacketTypeContainer
    {
        private static readonly PacketObject[] packetTypeList = 
        {
            new PacketObject(PacketType.UNKNOWN,             0, new List<Type>()),
            new PacketObject(PacketType.C_LOGIN,             4, new List<Type>{typeof(byte), typeof(string), typeof(byte), typeof(string)}),
            new PacketObject(PacketType.S_LOGIN_SUCCESS,     8, new List<Type>{typeof(UInt32), typeof(byte), typeof(string), typeof(byte), typeof(string), typeof(byte), typeof(string), typeof(UInt32)}),
            new PacketObject(PacketType.S_LOGIN_FAILED,      1, new List<Type>{typeof(byte)}),
            new PacketObject(PacketType.C_REGISTER,          8, new List<Type>{typeof(byte), typeof(string), typeof(byte), typeof(string), typeof(byte), typeof(string), typeof(byte), typeof(string)}),
            new PacketObject(PacketType.S_REGISTER_FAILED,   1, new List<Type>{typeof(byte)}),
            new PacketObject(PacketType.C_SEND_MESSAGE,      3, new List<Type>{typeof(UInt32), typeof(byte), typeof(string)}),
            new PacketObject(PacketType.S_SEND_MESSAGE,      3, new List<Type>{typeof(UInt32), typeof(byte), typeof(string)}),
            new PacketObject(PacketType.S_MESSAGE_FAILED,    2, new List<Type>{typeof(UInt32), typeof(byte)}),
            new PacketObject(PacketType.C_FRIEND_REQUEST,    1, new List<Type>{typeof(UInt32)}),
            new PacketObject(PacketType.C_FRIEND_REMOVE,     1, new List<Type>{typeof(UInt32)}),
            new PacketObject(PacketType.C_FRIEND_IGNORE,     1, new List<Type>{typeof(UInt32)}),
            new PacketObject(PacketType.S_FRIEND,            7, new List<Type>{typeof(UInt32), typeof(byte), typeof(string), typeof(byte), typeof(string), typeof(byte), typeof(string)})
        };

        public static PacketObject GetElement(PacketType packetType)
        {
            return packetTypeList[(UInt16)packetType];
        }

        public static int CountElements(PacketType packetType)
        {
            return packetTypeList[(UInt16)packetType].Size;
        }
    }

    public struct PacketObject
    {
        public PacketType PacketType { get; private set; }
        public int Size { get; private set; }
        public List<Type> DataList { get; private set; }
        public PacketObject(PacketType packetType, int size, List<Type> dataList)
            : this()
        {
            this.PacketType = packetType;
            this.Size = size;
            this.DataList = dataList;
        }
    }
}
