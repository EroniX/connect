﻿using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Collections.Generic;

namespace Shared
{
    public class Connector
    {
        private ManualResetEvent sending;
        private Socket socket;

        public Connector(Socket socket)
        {
            this.sending = new ManualResetEvent(false);
            this.socket = socket;
        }

        public Socket Socket
        {
            get
            {
                return this.socket;
            }
        }

        public ManualResetEvent Sending
        {
            get
            {
                return this.sending;
            }
        }

        public static bool IsConnected(Socket socket)
        {
            if (socket == null || socket.Connected == false)
            {
                return false;
            }

            bool part1 = socket.Poll(1000, SelectMode.SelectRead);
            bool part2 = (socket.Available == 0);
            if (part1 && part2) {
                return false;
            }

            return true;
        }

        public static void Disconnect(Socket socket)
        {
            if (Connector.IsConnected(socket))
            {
                socket.Shutdown(SocketShutdown.Both);
                socket.Close();
            }
        }
    }
}
