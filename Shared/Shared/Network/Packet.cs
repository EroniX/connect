﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace Shared
{
    public class Packet
    {
        public static readonly int DEFAULT_STRING_LENGTH = 510;
        public Client Client { get; private set; }
        public PacketObject PacketObject { get; set; }
        private byte[] Content { get; set; }
        private UInt16 Size { get; set; }

        public Packet(Client client, byte[] content, PacketType packetType)
        {
            this.Client = client;
            this.Content = content;
            this.PacketObject = PacketTypeContainer.GetElement(packetType);
        }

        public T Get<T>(int index)
        {
            if(index >= this.PacketObject.Size)
            {
                return default(T);
            }

            Type myType = this.PacketObject.DataList[index];
            if(myType != typeof(T))
            {
                return default(T);
            }

            int prevIndex = 0;
            int prevCount = 0;

            for (int i = 0; i < this.PacketObject.Size && i != index; i++)
            {
                Type type = this.PacketObject.DataList[i];
                int thisIndex = prevIndex + prevCount;

                if(type == typeof(string) && i != 0)
                {
                    Type prevType = this.PacketObject.DataList[i - 1];
                    byte[] part = Content.Skip(prevIndex).Take(prevCount).ToArray();
                    int length = Convert.ToInt32(ConvertByte.Get(part, prevType));
                    prevIndex = thisIndex;
                    prevCount = length;
                }
                else
                {
                    prevIndex = thisIndex;
                    prevCount = Marshal.SizeOf(this.PacketObject.DataList[i]);
                }
            }
            
            byte[] contentPart;
            if (myType == typeof(string) && index != 0)
            {
                Type prevType = this.PacketObject.DataList[index - 1];
                byte[] part = Content.Skip(prevIndex).Take(prevCount).ToArray();
                int length = Convert.ToInt32(ConvertByte.Get(part, prevType));
                contentPart = Content.Skip(prevIndex + prevCount).Take(length).ToArray();
            }
            else
            {
                int length = Marshal.SizeOf(this.PacketObject.DataList[index]);
                contentPart = Content.Skip(prevIndex + prevCount).Take(length).ToArray();
            }

            return (T)ConvertByte.Get(contentPart, myType);
        }
    }
}
