﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared
{
    public class Handler
    {
        public static Action<Packet> GetHandler(List<HandlerObject> handlerList, PacketType packetType)
        {
            if (handlerList.Exists(n => n.packetType == packetType))
            {
                HandlerObject handler = handlerList.Find(n => { return n.packetType == packetType; });
                return handler.action;
            }

            return null;
        }
    }

    public struct HandlerObject
    {
        public PacketType packetType { get; private set; }

        public Action<Packet> action { get; private set; }

        public HandlerObject(PacketType packetType, Action<Packet> action)
            : this()
        {
            this.packetType = packetType;
            this.action = action;
        }       
    }
}
