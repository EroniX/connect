﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared
{
    public class MyAccount : Account
    {
        public UInt32 LastMessageID { get; private set; }

        public MyAccount(UInt32 id, string username, string name, string mood, UInt32 lastMessageID)
            : base(id, username, name, mood)
        {
            this.LastMessageID = lastMessageID;
        }
    }
}
