﻿using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Collections.Generic;
using Shared;

namespace User
{
    public class ClientConnectionMgr : ConnectionMgr
    {
        private static ClientConnectionMgr cConnection;
        public static Client Me { get; private set; }

        public static void Start()
        {
            cConnection = new ClientConnectionMgr();
            cConnection.SetUp(new User.Handlers.ClientHandler());

            try
            {
                Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                socket.NoDelay = true;

                Me = new Client(socket);
                Me.Socket.BeginConnect(cConnection.connection.CreateEndPoint(), new AsyncCallback(cConnection.ConnectCallback), Me.Socket);
            }
            catch
            { }
        }

        private void ConnectCallback(IAsyncResult ar)
        {
            try
            {
                Me.Socket.EndConnect(ar);
                Receive(Me);
            }
            catch
            {
                // @TODO
                //Stop();
            }
        }
    }
}
