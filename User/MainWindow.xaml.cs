﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using Shared;
using User;
using User.Network;
using User.Tasks;
using User.Contents;
using User.Contents.Managers.Register;
using User.Contents.Managers.Login;
using User.Contents.Managers.Chat;
using User.Contents.Managers;

namespace User
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            MainMgr.Initialize();
            LoginMgr.Create();

            //if (ConnectionMgr.Connected == false) {
                ReceiverTask.Start();
            //}
        }

        private void WindowRegisterButton_Click(object sender, RoutedEventArgs e)
        {
        }
    }
}
