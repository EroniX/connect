﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Shared;
using Shared.Converting;
using User.Network;
using User.Contents.Managers.Register;
using User.Contents.Managers.Login;

namespace User.Contents
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class LoginUC : UserControl
    {
        public LoginUC()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string username = @usernameBox.Text;
            string password = @passwordBox.Text;

            //ClientConnectionMgr.Me.Account.TryLogIn(username, password);
        }

        private void windowRegister_Click(object sender, RoutedEventArgs e)
        {
            RegisterMgr.Create();
        }
    }
}
