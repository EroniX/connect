﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Shared;
using Shared.Converting;
using User.Network;
using User.Contents.Managers.Register;
using User.Contents.Managers.Login;

namespace User.Contents
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class RegisterUC : UserControl
    {
        public RegisterUC()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string username = usernameBox.Text;
            string password = passwordBox.Text;
            string name = nameBox.Text;
            string email = emailBox.Text;

            ConnectionMgr.Me.Account.TryRegister(username, password, name, email);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            LoginMgr.Create();
        }
    }
}
