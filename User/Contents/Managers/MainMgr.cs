﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using System.Threading;

namespace User.Contents.Managers
{
    public class MainMgr
    {
        private static MainWindow mainWindow;
        private static int mainThreadID;

        public static void Initialize()
        {
            MainMgr.mainWindow = Application.Current.MainWindow as MainWindow;
            MainMgr.mainThreadID = Thread.CurrentThread.ManagedThreadId;
        }

        public static void CreateContent(UserControl uc)
        {
            Action action = new Action(
                     () => { MainMgr.mainWindow.MainGrid.Children.Add(uc); });
            UpdateContent(action);
        }

        public static void RemoveContent(UserControl uc)
        {
            Action action = new Action(
                            () => { MainMgr.mainWindow.MainGrid.Children.Remove(uc); });
            UpdateContent(action);
        }

        public static void UpdateContent(Action action)
        {
            if (MainMgr.IsMainThread()) {
                action.Invoke();
            }
            else {
                mainWindow.Dispatcher.Invoke(DispatcherPriority.Normal, action);
            }
        }

        public static T UpdateContent<T>(Func<T> func) where T : UserControl
        {
            if (MainMgr.IsMainThread()) {
                return func.Invoke();
            }
            else {
                return mainWindow.Dispatcher.Invoke(func);
            }
        }

        public static bool IsMainThread()
        {
            return (MainMgr.mainThreadID == Thread.CurrentThread.ManagedThreadId);
        }
    }
}
