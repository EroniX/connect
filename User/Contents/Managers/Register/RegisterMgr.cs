﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using User.Contents.Managers;
using User.Contents.Managers.Chat;
using User.Contents.Managers.Login;

namespace User.Contents.Managers.Register
{
    class RegisterMgr
    {
        private static bool active = false;
        private static RegisterUC registerUC;

        public static void Create()
        {
            LoginMgr.Remove();
            ChatMgr.Remove();

            registerUC = MainMgr.UpdateContent<RegisterUC>(
                () => { return new RegisterUC { Name = "registerUC" }; });
            MainMgr.CreateContent(registerUC);
            active = true;
        }

        public static void Remove()
        {
            if (active == true)
            {
                MainMgr.RemoveContent(registerUC);
                registerUC = null;
                active = false;
            }
        }

        public static void ErrorMessage(byte errorCode)
        {
            // Inappropriate character in username or password
            if (errorCode == 0)
            {
                Action action = new Action(
                        () => { registerUC.errorMessage.Content = "Use only letter or number!"; });
                MainMgr.UpdateContent(action);
            }
            // Username exists
            else
            {
                Action action = new Action(
                        () => { registerUC.errorMessage.Content = "Username is already exist!"; });
                MainMgr.UpdateContent(action);
            }
        }
    }
}
