﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using User;
using User.Contents;
using User.Contents.Managers;
using User.Contents.Managers.Login;
using User.Contents.Managers.Register;

namespace User.Contents.Managers.Chat
{
    class ChatMgr
    {
        private static bool active = false;
        private static ChatUC chatUC;

        public static void Create()
        {
            RegisterMgr.Remove();
            LoginMgr.Remove();

            chatUC = MainMgr.UpdateContent<ChatUC>(
                () => { return new ChatUC { Name = "chatUC" }; });
            MainMgr.CreateContent(chatUC);
            active = true;
        }

        public static void Remove()
        {
            if (active == true)
            {
                MainMgr.RemoveContent(chatUC);
                chatUC = null;
                active = false;
            }
        }
    }
}
