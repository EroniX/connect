﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using User.Contents.Managers;
using User.Contents.Managers.Chat;
using User.Contents.Managers.Register;

namespace User.Contents.Managers.Login
{
    public class LoginMgr
    {
        private static bool active = false;
        private static LoginUC loginUC;
        public static void Create()
        {
            RegisterMgr.Remove();
            ChatMgr.Remove();

            loginUC = MainMgr.UpdateContent<LoginUC>(
                () => { return new LoginUC { Name = "loginUC" }; });
            MainMgr.CreateContent(loginUC);
            active = true;
        }
        public static void Remove()
        {
            if (active == true)
            {
                MainMgr.RemoveContent(loginUC);
                loginUC = null;
                active = false;
            }
        }
        public static void ErrorMessage(byte errorCode)
        {
            // Wrong username or password
            if (errorCode == 0)
            {
                Action action = new Action(
                        () => { loginUC.errorMessage.Content = "Wrong username or password!"; });
                MainMgr.UpdateContent(action);
            }
            // Other Error
            else
            {
                Action action = new Action(
                        () => { loginUC.errorMessage.Content = "Something went wrong, try later!"; });
                MainMgr.UpdateContent(action);
            }
        }
    }
}
