﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared;
using Shared.Converting;

namespace User.Network
{
    public class AccountMgr
    {
        public bool LoggedIn { get; private set; }
        public MyAccount Datas { get; private set; }

        public AccountMgr()
        {
            this.LoggedIn = false;
        }

        public void LogIn(MyAccount account)
        {
            this.Datas = account;
            this.LoggedIn = true;
        }

        public void Logout()
        {
            this.Datas = null;
        }

        public void TryLogIn(string username, string password)
        {
           /* if (LoggedIn == false)
            {
                PacketBuilder packet = new PacketBuilder(PacketType.C_LOGIN);
                packet.Set(0, ConvertValue.GetShortStringLength(username));
                packet.Set(1, username);
                packet.Set(2, ConvertValue.GetShortStringLength(password));
                packet.Set(3, password);
                Client.Send(packet);
            }*/
        }

        public void TryRegister(string username, string password, string name, string email)
        {
            if (LoggedIn == false)
            {
               /* PacketBuilder packet = new PacketBuilder(PacketType.C_REGISTER);
                packet.Set<byte>(0, Convert.ToByte(ConvertValue.GetShortStringLength(username)));
                packet.Set<string>(1, username);
                packet.Set<byte>(2, Convert.ToByte(ConvertValue.GetShortStringLength(password)));
                packet.Set<string>(3, password);
                packet.Set<byte>(4, Convert.ToByte(ConvertValue.GetShortStringLength(name)));
                packet.Set<string>(5, name);
                packet.Set<byte>(6, Convert.ToByte(ConvertValue.GetShortStringLength(email)));
                packet.Set<string>(7, email);
                Client.Send(packet);*/
            }
        }
    }
}
