﻿//using System;
//using System.Linq;
//using System.Net;
//using System.Net.Sockets;
//using System.Text;
//using System.Threading;
//using System.Collections.Generic;
//using Shared;
//using User.Handlers;

//namespace User.Network
//{
//    public class Client : Connector
//    {
//        public AccountMgr Account { get; private set; }
//        public static Client Me { get; private set; }
//        private static bool connected = false;
//        private static Connection connection;

//        public static bool Connected 
//        { 
//            get 
//            {
//                return (connected && Me != null && Connector.IsConnected(Me.Socket));
//            }
//        }

//        public Client(Socket socket)
//            : base(socket)
//        {
//            this.Account = new AccountMgr();
//        }

//        public static void Start()
//        {
//            if (connected == true) {
//                return;
//            }

//            Client.connection = new Connection();
//            Client.connection.Initialize();

//            try
//            {
//                Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
//                socket.NoDelay = true;

//                Me = new Client(socket);
//                Me.Socket.BeginConnect(Client.connection.CreateEndPoint(), new AsyncCallback(ConnectCallback), Me.Socket);
//            }
//            catch
//            { }
//        }

//        public static void Stop()
//        {
//            connected = false;
//            Connector.Disconnect(Me.Socket);
//        }

//        private static void ConnectCallback(IAsyncResult ar)
//        {
//            try
//            {
//                Me.Socket.EndConnect(ar);
//                Receive();
//                connected = true;
//            }
//            catch
//            {
//                Stop();
//            }
//        }

//        private static void Receive()
//        {
//            try
//            {
//                Data data = new Data();
//                Me.Socket.BeginReceive(data.Buffer, 0, Data.BufferSize, 0,
//                    new AsyncCallback(ReceiveCallback), data);
//            }
//            catch
//            {
//                Stop();
//                return;
//            }
//        }

//        private static void ReceiveCallback(IAsyncResult ar)
//        {
//            Data data = (Data)ar.AsyncState;

//            int bytesRead = 0;
//            try
//            {
//                bytesRead = Me.Socket.EndReceive(ar);
//            }
//            catch
//            {
//                // Lost connection or Lost packet
//                Stop();
//                return;
//            }

//            if (bytesRead > 0)
//            {
//                data.Packet.AddRange(data.Buffer.Take(bytesRead));

//                // Check if we can get the size
//                if (data.GetSize() == 0 && data.Packet.Count() >= 2)
//                {
//                    data.SetSize();
//                }

//                // Check if the whole packet is arrived
//                if (data.GetSize() == data.Packet.Count())
//                {
//                    Receive();
//                    data.SetPacketType();
//                    Packet packet = new Packet(data.GetContent().ToArray(), data.GetPacketType());
//                    ClientHandler.HandlePacket(packet);
//                }

//                // Sticked packets
//                else if (data.GetSize() < data.Packet.Count())
//                {
//                    do
//                    {
//                        List<byte> myPacket = data.Packet.Take(data.GetSize()).ToList();
//                        Data myData = new Data(myPacket);

//                        myData.SetSize(data.GetSize());
//                        myData.SetPacketType();
                        
//                        Packet packet = new Packet(myData.GetContent().ToArray(), myData.GetPacketType());
//                        ClientHandler.HandlePacket(packet);

//                        data.Packet = data.Packet.Skip(data.GetSize()).ToList();
//                        if (data.Packet.Count() >= 2)
//                        {
//                            data.SetSize();
//                        }
//                    }
//                    while (data.GetSize() >= data.Packet.Count() && data.Packet.Count() != 0);

//                    if (data.GetSize() == 0)
//                    {
//                        Receive();
//                    }
//                    else
//                    {
//                        Me.Socket.BeginReceive(data.Buffer, 0, Data.BufferSize, 0,
//                            new AsyncCallback(ReceiveCallback), data);
//                    }
//                }
//                else
//                {
//                    Me.Socket.BeginReceive(data.Buffer, 0, Data.BufferSize, 0,
//                            new AsyncCallback(ReceiveCallback), data);
//                }
//            }
//        }

//        public static void Send(PacketBuilder packet)
//        {
//            try
//            {
//                Me.Sending.WaitOne(100);
//                Me.Sending.Reset();

//                List<byte> finish = packet.Finish();
//                Me.Socket.BeginSend(finish.ToArray(), 0, finish.Count(), 0,
//                    new AsyncCallback(SendCallback), null);
//            }
//            catch
//            {
//                Stop();
//            }
//        }

//        private static void SendCallback(IAsyncResult ar)
//        {
//            try
//            {
//                Me.Socket.EndSend(ar);
//                Me.Sending.Set();
//            }
//            catch
//            {
//                Stop();
//            }
//        }
//    }
//}
