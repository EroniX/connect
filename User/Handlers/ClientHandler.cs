﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared;
using Shared;

namespace User.Handlers
{
    public class ClientHandler : IHandler
    {
        private readonly List<HandlerObject> HandlerList = new List<HandlerObject>
        {   
            new HandlerObject(PacketType.S_LOGIN_SUCCESS,    BasicHandler.HandleLoginSuccess),
            new HandlerObject(PacketType.S_LOGIN_FAILED,     BasicHandler.HandleLoginFailed),
            new HandlerObject(PacketType.S_REGISTER_FAILED,  BasicHandler.HandleRegisterFailed)
        };

        public void HandleData(Packet packet)
        {
            Action<Packet> action = Handler.GetHandler(HandlerList, packet.PacketObject.PacketType);
            if (action != null)
            {
                if (packet != null)
                {
                    action.Invoke(packet);
                }
                else
                {
                    // Lost packet!
                }
            }
            else
            {
                action = BasicHandler.HandleUnknown;
                action.Invoke(null);
            }
        }
    }
}
