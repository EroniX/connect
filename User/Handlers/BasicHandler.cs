﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared;
using System.Windows;
using System.Windows.Controls;
using User.Contents.Managers.Register;
using User.Contents.Managers.Login;
using User.Contents.Managers.Chat;
using User.Network;

namespace User.Handlers
{
    class BasicHandler
    {
        public static void HandleUnknown(Packet packet)
        {
            Console.WriteLine("HandleUnknown");
        }

        public static void HandleLoginSuccess(Packet packet)
        {
            UInt32 id = packet.Get<UInt32>(0);
            string username = packet.Get<string>(2);
            string name = packet.Get<string>(4);
            string email = packet.Get<string>(6);
            UInt32 lastMessageId = packet.Get<UInt32>(7);

            MyAccount account = new MyAccount(id, username, name, email, lastMessageId);
            ConnectionMgr.Me.Account.LogIn(account);

            ChatMgr.Create();
        }

        public static void HandleLoginFailed(Packet packet)
        {
            byte errorCode = packet.Get<byte>(0);
            LoginMgr.ErrorMessage(errorCode);
        }

        public static void HandleRegisterFailed(Packet packet)
        {
            byte errorCode = packet.Get<byte>(0);
            RegisterMgr.ErrorMessage(errorCode);
        }
    }
}
