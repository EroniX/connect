﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared.Tasks;
using User.Network;
using System.Threading;

namespace User.Tasks
{
    class ReceiverTask
    {
        private static TaskMgr taskMgr;

        public static void Start()
        {
            ReceiverTask.taskMgr = new TaskMgr();
            ReceiverTask.taskMgr.Start(ClientConnectionMgr.Start);
        }

        public static void Stop()
        {
            ReceiverTask.taskMgr.Stop();
        }
    }
}
